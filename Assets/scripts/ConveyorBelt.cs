﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ConveyorBelt : MonoBehaviour 
{
 //   public GameObject anotherObject;


    public GameObject belt;
    public Transform endpoint;
    public float speed;
    

    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<CubeSc>().identity == 1)
        {
            other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint.position, speed * Time.deltaTime);

        }
    }


}
